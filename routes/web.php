<?php

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/', 'HomeController@home')->name('home');

Route::resource('send', 'SendController');
Route::get('/quick_send', 'SendController@quick_send_test');
Route::resource('userManagement', 'UserManagementController');

Route::resource('users', 'UserController');

Route::group(['namespace' => 'SMS'], function () {
    Route::resource('campain', 'CampainController');
    Route::resource('gateway', 'GatewayController');
    Route::resource('sendSMS', 'SendSMSController');
});

Route::group(['namespace' => 'Webhook'], function () {
    Route::post('/webhook/twilio', 'TwilioController@respond');
});
