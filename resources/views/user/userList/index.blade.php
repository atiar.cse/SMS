@extends('layouts.master')
@section('content')  
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         SMS User
      </h1>
      <ol class="breadcrumb">
         <li><a href="#"><i class="fa fa-comments"></i> SMS User</a></li>
         <li class="active">Forwarding Settings</li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content container-fluid">
      <div class="row">
         <div class="col-md-10 col-md-offset-1">
            <!-- Custom Tabs -->
            <div class="nav-tabs-custom">
               <ul class="nav nav-tabs">
                  <li class="active"><a href="#tab_1" data-toggle="tab"> Pending Request </a></li>
                  <li><a href="#tab_2" data-toggle="tab">Approved User</a></li>
                  <li><a href="#tab_3" data-toggle="tab">Not Approved User</a></li>
                  <li><a href="#tab_4" data-toggle="tab">Banned User</a></li>
               </ul>
               <div class="tab-content">
                  <div class="tab-pane active" id="tab_1">
                     <div class="box-body">
                        <table class="table table-bordered">
                           <thead>
                              <tr>
                                 <th>Sl.</th>
                                 <th>Name</th>
                                 <th>Email</th>
                                 <th>Action</th>
                              </tr>
                           </thead>
                           <tbody id="a_tbl_body">
                              <?php $i=1; ?>
                              @foreach($pending_users as $rowdata)
                              <tr>
                                 <td> {{$i++}} </td>
                                 <td>{{$rowdata->name}}</td>
                                 <td>{{$rowdata->email}}</td>
                                 <td>
                                    <a class="btn btn-info" href="/userManagement/{{$rowdata->id}}/edit?action=approve"><i class="fa fa-pencil-square-o"></i> Approve </a>
                                    <a class="btn btn-danger" href="/userManagement/{{$rowdata->id}}/edit?action=notApprove"><i class="fa fa-pencil-square-o"></i> Not Approve </a>
                                 </td>
                              </tr>
                              @endforeach
                           <tbody>
                        </table>
                     </div>
                  </div>
                  <!-- /.tab-pane -->
                  <div class="tab-pane" id="tab_2">
                     <div class="box-body">
                        <table class="table table-bordered">
                           <thead>
                              <tr>
                                 <th>Sl.</th>
                                 <th>Name</th>
                                 <th>Email</th>
                                 <th>Action</th>
                              </tr>
                           </thead>
                           <tbody id="a_tbl_body">
                              <?php $i=1; ?>
                              @foreach($approved_users as $rowdata)
                              <tr>
                                 <td> {{$i++}} </td>
                                 <td>{{$rowdata->name}}</td>
                                 <td>{{$rowdata->email}}</td>
                                 <td><a class="btn btn-danger" href="/userManagement/{{$rowdata->id}}/edit?action=ban"><i class="fa fa-pencil-square-o"></i> Ban </a></td>
                              </tr>
                              @endforeach
                           <tbody>
                        </table>
                     </div>
                  </div>
                  <!-- /.tab-pane -->
                  <div class="tab-pane" id="tab_3">
                     <div class="box-body">
                        <table class="table table-bordered">
                           <thead>
                              <tr>
                                 <th>Sl.</th>
                                 <th>Name</th>
                                 <th>Email</th>
                                 <th>Action</th>
                              </tr>
                           </thead>
                           <tbody id="a_tbl_body">
                              <?php $i=1; ?>
                              @foreach($not_approved_users as $rowdata)
                              <tr>
                                 <td> {{$i++}} </td>
                                 <td>{{$rowdata->name}}</td>
                                 <td>{{$rowdata->email}}</td>
                                 <td><a class="btn btn-info" href="/userManagement/{{$rowdata->id}}/edit?action=approve"><i class="fa fa-pencil-square-o"></i> Approve </a></td>
                              </tr>
                              @endforeach
                           <tbody>
                        </table>
                     </div>
                  </div>
                  <!-- /.tab-pane -->
                  <!-- /.tab-pane -->
                  <div class="tab-pane" id="tab_4">
                     <div class="box-body">
                        <table class="table table-bordered">
                           <thead>
                              <tr>
                                 <th>Sl.</th>
                                 <th>Name</th>
                                 <th>Email</th>
                                 <th>Action</th>
                              </tr>
                           </thead>
                           <tbody id="a_tbl_body">
                              <?php $i=1; ?>
                              @foreach($banned_users as $rowdata)
                              <tr>
                                 <td> {{$i++}} </td>
                                 <td>{{$rowdata->name}}</td>
                                 <td>{{$rowdata->email}}</td>
                                 <td><a class="btn btn-danger" href="/userManagement/{{$rowdata->id}}/edit?action=unban"><i class="fa fa-pencil-square-o"></i> Un-Ban </a></td>
                              </tr>
                              @endforeach
                           <tbody>
                        </table>
                     </div>
                  </div>
                  <!-- /.tab-pane -->
               </div>
               <!-- /.tab-content -->
            </div>
            <!-- nav-tabs-custom -->
         </div>
      </div>
   </section>
   <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<script type="text/javascript">
   function deleteRow(obj) {
      $(obj).closest('tr').remove();
   }

   function addItemtoListAutoReply(){
      var row = '<tr> <td><input type="text" name="a_message[]" class="form-control" required></td><td><input type="text" name="a_response_time[]" class="form-control"></td><td><a onclick="deleteRow(this)" class="btn btn-danger"><i class=""></i> Delete </a></td></tr>';

      $("#a_tbl_body").append(row);
   }

   function addItemtoListIntReply(){
      var row = '<tr> <td><input type="text" name="i_message[]" class="form-control" required></td><td><input type="text" name="i_response_time[]" class="form-control"></td><td><a onclick="deleteRow(this)" class="btn btn-danger"><i class=""></i> Delete </a></td></tr>';

      $("#i_tbl_body").append(row);
   }

   function addItemtoListNewsLetter(){
      var row = '<tr> <td><input type="text" name="n_message[]" class="form-control" required></td><td><input type="text" name="n_response_time[]" class="form-control"></td><td><a onclick="deleteRow(this)" class="btn btn-danger"><i class=""></i> Delete </a></td></tr>';

      $("#n_tbl_body").append(row);
   }
</script>
@endsection

