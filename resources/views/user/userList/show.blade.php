@extends('layouts.master')
@section('content')  
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         SMS Campain
      </h1>
      <ol class="breadcrumb">
         <li><a href="#"><i class="fa fa-comments"></i> SMS Campain</a></li>
         <li class="active">Forwarding Settings</li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content container-fluid">
      <div class="row">
         <!-- left column -->
         <div class="col-md-10 col-md-offset-1">
            <!-- general form elements -->
            <div class="box box-primary">
               <!-- /.box-header -->
               <div class="box-body">
                  <div class="form-group">
                     <label>Campain Name : {{$campain->name}}</label>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="row">
         <div class="col-md-10 col-md-offset-1">
            <!-- Custom Tabs -->
            <div class="nav-tabs-custom">
               <ul class="nav nav-tabs">
                  <li class="active"><a href="#tab_1" data-toggle="tab">Auto Reply</a></li>
                  <li><a href="#tab_2" data-toggle="tab">Intelligence Reply</a></li>
                  <li><a href="#tab_3" data-toggle="tab">Newsletter</a></li>
               </ul>
               <div class="tab-content">
                  <div class="tab-pane active" id="tab_1">
                     <div class="box-body">
                        <table class="table table-bordered">
                           <thead>
                              <tr>
                                 <th>Message</th>
                                 <th style="width: 150px;">Time(In Second)</th>
                              </tr>
                           </thead>
                           <tbody id="a_tbl_body">
                              @foreach($campain->getAutoMessageInfoRow as $rowdata)
                              <tr>
                                 <td> {{$rowdata->a_message}} </td>
                                 <td> {{$rowdata->a_response_time}} </td>
                              </tr>
                              @endforeach
                           <tbody>
                        </table>
                     </div>
                  </div>
                  <!-- /.tab-pane -->
                  <div class="tab-pane" id="tab_2">
                     <div class="box-body">
                        <table class="table table-bordered">
                           <thead>
                              <tr>
                                 <th>Message</th>
                                 <th style="width: 150px;">Time(In Second)</th>
                              </tr>
                           </thead>
                           <tbody id="i_tbl_body">
                              @foreach($campain->getIntelligenceMessageInfoRow as $rowdata)
                              <tr>
                                 <td>{{$rowdata->i_message}}</td>
                                 <td>{{$rowdata->i_response_time}}</td>
                              </tr>
                              @endforeach
                           <tbody>
                        </table>
                     </div>
                  </div>
                  <!-- /.tab-pane -->
                  <div class="tab-pane" id="tab_3">
                     <div class="box-body">
                        <table class="table table-bordered">
                           <thead>
                              <tr>
                                 <th>Message</th>
                                 <th style="width: 150px;">Time(In Second)</th>
                              </tr>
                           </thead>
                           <tbody id="n_tbl_body">
                              @foreach($campain->getNewsLetterMessageInfoRow as $rowdata)
                              <tr>
                                 <td>{{$rowdata->n_message}}</td>
                                 <td>{{$rowdata->n_response_time}}</td>
                              </tr>
                              @endforeach
                           <tbody>
                        </table>
                     </div>
                  </div>
                  <!-- /.tab-pane -->
               </div>
               <!-- /.tab-content -->
            </div>
            <!-- nav-tabs-custom -->
         </div>
      </div>
      <div class="box-footer text-center">
         <a href="/campain" class="btn btn-danger"><i class="fa fa-arrow-left"></i> Back To List View</a>
      </div>
   </section>
   <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection

