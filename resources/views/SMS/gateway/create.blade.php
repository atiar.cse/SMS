@extends('layouts.master')
@section('content')  
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         Gateway
         <small>Asign Campain</small>
      </h1>
      <ol class="breadcrumb">
         <li><a href="#"><i class="fa fa-comments"></i> SMS Gateway</a></li>
         <li class="active">Forwarding Settings</li>
      </ol>
   </section>
   <!-- Main content -->
   @include('layouts.messages')
   <section class="content container-fluid">
      <div class="row">
         <!-- left column -->
         <div class="col-md-12">
            @if ($campain_count <= 0)
            <div class="alert alert-warning">
               <strong>Warning!</strong> Please Entry Campain First <a class="btn btn-xs btn-info" href="/campain/create"><i class="fa fa-plus"></i> Add Campain </a>
            </div>
            @endif
            <!-- general form elements -->
            <div class="box box-primary">
               <!-- /.box-header -->
               <!-- form start -->
               <form method="POST" action="/gateway" enctype="multipart/form-data">
               {{csrf_field()}}
                  <div class="box-body">
                     <div class="form-group">
                        <label>Gateway Name</label>
                        <select name="gateway" class="form-control" required>
                           <option value="1">Twilio</option>
                        </select>
                     </div>                  
                     <div class="form-group">
                        <label>Account SID/Key</label>
                        <input name="sid_key" class="form-control" type="text" maxlength="100" placeholder="Enter SID/Key" required>
                     </div>                  
                     <div class="form-group">
                        <label>Auth Token/Secret</label>
                        <input name="token_secret" class="form-control" type="text" maxlength="100" placeholder="Enter Token/Secret" required>
                     </div>                  
                     <div class="form-group">
                        <label>Phone Number <small> (e.g +88017xxXXxxXX)</small></label>
                        <input name="phone_number" class="form-control" type="text" maxlength="20" placeholder="Enter Phone Number e.g +88017xxXXxxXX" required>
                     </div>
                     <div class="form-group">
                        <label>Campain</label>
                        <select name="campain_id" class="form-control" required>
                           <option value="">Please Select</option>
                           @foreach($campains as $rowdata)
                           <option value="{{$rowdata->id}}">{{$rowdata->name}}</option>
                           @endforeach
                        </select>
                     </div>
                     <div class="form-group">
                        <label> Remarks </label>
                        <textarea name="remarks" type="text" class="form-control" rows="2"></textarea>
                     </div>
                  </div>
                  <!-- /.box-body -->
                  <div class="box-footer text-center">
                     <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
               </form>

            </div>
            <!-- /.box -->
         </div>
         <!--/.col (left) -->
         <div class="col-md-10 col-md-offset-1">
            <div class="alert alert-info">
               <strong>Info</strong> Please Update this URL http://sms.fifolab.com/webhook/twilio into your twilio account with that respected phone number. And make sure that "HTTP POST" is selected.
            </div>
            <img class="profile-pic img-responsive" src="{{ asset('/img/Twilio.png')}}" alt="">
         </div>
      </div>
   </section>
   <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<script type="text/javascript">
   
</script>
@endsection

