@extends('layouts.master')
@section('content')  
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         Gateway
         <small>Update</small>
      </h1>
      <ol class="breadcrumb">
         <li><a href="#"><i class="fa fa-comments"></i> SMS Gateway</a></li>
         <li class="active">Forwarding Settings</li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content container-fluid">
      <div class="row">
         <!-- left column -->
         <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
               <!-- /.box-header -->
               <!-- form start -->
               <form method="POST" action="/gateway/{{$gateway->id}}" enctype="multipart/form-data">
               {{csrf_field()}}
               {{method_field('PUT')}}
                  <div class="box-body">
                     <div class="form-group">
                        <label>Gateway Name</label>
                        <select name="gateway" class="form-control" required>
                           <option value="1">Twilio</option>
                        </select>
                     </div>                  
                     <div class="form-group">
                        <label>Account SID/Key</label>
                        <input name="sid_key" class="form-control" type="text" maxlength="100" placeholder="Enter SID/Key" value="{{$gateway->sid_key}}" required>
                     </div>                  
                     <div class="form-group">
                        <label>Auth Token/Secret</label>
                        <input name="token_secret" class="form-control" type="text" maxlength="100" placeholder="Enter Token/Secret" value="{{$gateway->token_secret}}" required>
                     </div>                  
                     <div class="form-group">
                        <label>Phone Number</label>
                        <input name="phone_number" class="form-control" type="text" maxlength="20" placeholder="Enter Phone Number e.g +88017xxXXxxXX" value="{{$gateway->phone_number}}" required readonly>
                     </div>
                     <div class="form-group">
                        <label>Campain</label>
                        <select name="campain_id" class="form-control" required>
                           <option value="">Please Select</option>
                           @foreach($campains as $rowdata)
                           <option value="{{$rowdata->id}}" @if($gateway->campain_id == $rowdata->id) selected @endif >{{$rowdata->name}}</option>
                           @endforeach
                        </select>
                     </div>
                     <div class="form-group">
                        <label> Remarks </label>
                        <textarea name="remarks" type="text" class="form-control" rows="2">{{$gateway->remarks}}</textarea>
                     </div>
                  </div>
                  <!-- /.box-body -->
                  <div class="box-footer text-center">
                     <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
               </form>
            </div>
            <!-- /.box -->
         </div>
         <!--/.col (left) -->
      </div>
   </section>
   <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<script type="text/javascript">
   
</script>
@endsection

