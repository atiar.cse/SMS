@extends('layouts.master')
@section('content')  
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         Gateway
         <small>ALL</small>
      </h1>
      <ol class="breadcrumb">
         <li><a href="#"><i class="fa fa-comments"></i> SMS Gateway</a></li>
         <li class="active">Forwarding Settings</li>
      </ol>
   </section>
   <!-- Main content -->
   @include('layouts.messages')
   <section class="content container-fluid">
      <div class="row">
         <div class="col-md-12">
            <div class="box">
               <div class="box-header with-border">
                  <h3 class="box-title">Forwarding Settings</h3>
                    <a class="btn btn-xs btn-info pull-right" href="/gateway/create"><i class="fa fa-plus"></i> Add New
                    </a>                  
               </div>
               <!-- /.box-header -->
               <div class="box-body">
                  <table class="table table-bordered">
                     <thead>
                        <tr>
                           <th>Sl.</th>
                           <th>Gateway Name</th>
                           <th>SID/Key</th>
                           <th>Token/Secret</th>
                           <th>Phone Number</th>
                           <th>Balance</th>
                           <th>Campain</th>
                           <th>Remarks</th>
                           <th>Status</th>
                           <th>Actions</th>
                        </tr>
                     </thead>
                     <tbody>
                        <?php $i=1; ?>
                        @foreach($gateways as $rowdata)
                        <tr>
                           <td>{{$i++}}</td>
                           <td>Twilio</td>
                           <td>{{$rowdata->sid_key}}</td>
                           <td>{{$rowdata->token_secret}}</td>
                           <td>{{$rowdata->phone_number}}</td>
                           <td></td>
                           <td>{{$rowdata->getCampainInfoRow->name}}</td>
                           <td>{{$rowdata->remarks}}</td>
                           <td>
                              @if($rowdata->status == 1)
                                 <h3 style="color: green;" class="text-bold;">Enabled</h3>
                              @elseif($rowdata->status == 2)
                                 <h3 style="color: red;" class="text-bold;">Disabled</h3>
                              @endif
                           </td>
                           <td>
                              <a href="/gateway/{{$rowdata->id}}/edit" class="btn btn-xs btn-primary"><i class="fa fa-edit"></i> Edit </a>
                              @if($rowdata->status == 1)
                              <a href="/gateway/{{$rowdata->id}}?status=2" class="btn btn-xs btn-danger"><i class="fa fa-ban"></i> Disable Now </a>
                              @elseif($rowdata->status == 2)
                              <a href="/gateway/{{$rowdata->id}}?status=1" class="btn btn-xs btn-info"><i class="fa fa-check"></i> Enable Now </a>
                              @endif
                           </td>
                        </tr>
                        @endforeach
                     </tbody>
                  </table>
               </div>
               <!-- /.box-body -->
               <div class="box-footer clearfix">
                  <ul class="pagination pagination-sm no-margin pull-right">
                     <li><a href="#">«</a></li>
                     <li><a href="#">1</a></li>
                     <li><a href="#">2</a></li>
                     <li><a href="#">3</a></li>
                     <li><a href="#">»</a></li>
                  </ul>
               </div>
            </div>
         </div>         
      </div>
   </section>
   <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection