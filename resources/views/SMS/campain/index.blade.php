@extends('layouts.master')
@section('content')  
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         SMS Campain
         <small>ALL</small>
      </h1>
      <ol class="breadcrumb">
         <li><a href="#"><i class="fa fa-comments"></i> SMS Campain </a></li>
         <li class="active">Forwarding Settings</li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content container-fluid">
      <div class="row">
         <div class="col-md-12">
            <div class="box">
               <div class="box-header with-border">
                  <h3 class="box-title">Forwarding Settings</h3>
                    <a class="btn btn-xs btn-info pull-right" href="/campain/create"><i class="fa fa-plus"></i> Add New
                    </a>                  
               </div>
               <!-- /.box-header -->
               <div class="box-body">
                  <table class="table table-bordered">
                     <thead>
                        <tr>
                           <th>Sl.</th>
                           <th>Campain Name</th>
                           <th>Count</th>
                           <th>Actions</th>
                        </tr>
                     </thead>
                     <tbody>
                        <?php $i=1; ?>
                        @foreach($campains as $rowdata)
                        <tr>
                           <td>{{$i++}}</td>
                           <td>{{$rowdata->name}}</td>
                           <td>5</td>
                           <td>
                              <a href="/campain/{{$rowdata->id}}" class="btn btn-xs btn-info"><i class="fa fa-eye"></i> Show </a>
                              <a href="/campain/{{$rowdata->id}}/edit" class="btn btn-xs btn-primary"><i class="fa fa-edit"></i> Edit </a>
                              <a href="#" class="btn btn-xs btn-success"><i class="fa fa-cog"></i> Manage </a>
                              <a href="#" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i> Delete</a>
                           </td>
                        </tr>
                        @endforeach
                     </tbody>
                  </table>
               </div>
               <!-- /.box-body -->
               <div class="box-footer clearfix">
                  <ul class="pagination pagination-sm no-margin pull-right">
                     <li><a href="#">«</a></li>
                     <li><a href="#">1</a></li>
                     <li><a href="#">2</a></li>
                     <li><a href="#">3</a></li>
                     <li><a href="#">»</a></li>
                  </ul>
               </div>
            </div>
         </div>         
      </div>
   </section>
   <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection