@extends('layouts.master')
@section('content')  
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         SMS Campain
      </h1>
      <ol class="breadcrumb">
         <li><a href="#"><i class="fa fa-comments"></i> SMS Campain</a></li>
         <li class="active">Forwarding Settings</li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content container-fluid">
      <form method="POST" action="/campain" enctype="multipart/form-data">
         {{csrf_field()}}
         <div class="row">
            <!-- left column -->
            <div class="col-md-12">
               <!-- general form elements -->
               <div class="box box-primary">
                  <!-- /.box-header -->
                  <div class="box-body">
                     <div class="form-group">
                        <label>Campain Name * </label>
                        <input name="name" class="form-control" type="text" placeholder="Enter Name" required>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="row">
            <div class="col-md-12">
               <!-- Custom Tabs -->
               <div class="nav-tabs-custom">
                  <ul class="nav nav-tabs">
                     <li class="active"><a href="#tab_1" data-toggle="tab">Auto Reply</a></li>
                     <li><a href="#tab_2" data-toggle="tab">Intelligence Reply</a></li>
                     <li><a href="#tab_3" data-toggle="tab">Newsletter</a></li>
                  </ul>
                  <div class="tab-content">
                     <div class="tab-pane active" id="tab_1">
                        <div class="box-body">
                           <table class="table table-bordered">
                              <thead>
                                 <tr>
                                    <th>Message * </th>
                                    <th style="width: 150px;">Time(In Second) * </th>
                                    <th style="width: 100px;">Action</th>
                                 </tr>
                              </thead>
                              <tbody id="a_tbl_body">
                                 <tr>
                                    <td><input type="text" name="a_message[]" class="form-control" required></td>
                                    <td><input type="text" name="a_response_time[]" class="form-control" required></td>
                                    <td><a onclick="deleteRow(this)" class="btn btn-danger"><i class=""></i> Delete </a></td>
                                 </tr>
                              <tbody>
                           </table>
                           <div class="pull-right"><a onclick="addItemtoListAutoReply()" class="btn btn-info"><i class="fa fa-plus"></i> Add </a></div>
                        </div>
                     </div>
                     <!-- /.tab-pane -->
                     <div class="tab-pane" id="tab_2">
                        <div class="box-body">
                           <table class="table table-bordered">
                              <thead>
                                 <tr>
                                    <th>Message * </th>
                                    <th style="width: 150px;">Time(In Second) * </th>
                                    <th style="width: 100px;">Action</th>
                                 </tr>
                              </thead>
                              <tbody id="i_tbl_body">
                                 <tr>
                                    <td><input type="text" name="i_message[]" class="form-control" required></td>
                                    <td><input type="text" name="i_response_time[]" class="form-control" required></td>
                                    <td><a onclick="deleteRow(this)" class="btn btn-danger"><i class=""></i> Delete </a></td>
                                 </tr>
                              <tbody>
                           </table>
                           <div class="pull-right"><a onclick="addItemtoListIntReply()" class="btn btn-info"><i class="fa fa-plus"></i> Add </a></div>
                        </div>
                     </div>
                     <!-- /.tab-pane -->
                     <div class="tab-pane" id="tab_3">
                        <div class="box-body">
                           <table class="table table-bordered">
                              <thead>
                                 <tr>
                                    <th>Message</th>
                                    <th style="width: 150px;">Time(In Second)</th>
                                    <th style="width: 100px;">Action</th>
                                 </tr>
                              </thead>
                              <tbody id="n_tbl_body">
                                 <tr>
                                    <td><input type="text" name="n_message[]" class="form-control"></td>
                                    <td><input type="text" name="n_response_time[]" class="form-control"></td>
                                    <td><a onclick="deleteRow(this)" class="btn btn-danger"><i class=""></i> Delete </a></td>
                                 </tr>
                              <tbody>
                           </table>
                           <div class="pull-right"><a onclick="addItemtoListNewsLetter()" class="btn btn-info"><i class="fa fa-plus"></i> Add </a></div>
                        </div>
                     </div>
                     <!-- /.tab-pane -->
                  </div>
                  <!-- /.tab-content -->
               </div>
               <!-- nav-tabs-custom -->
            </div>
         </div>
         <div class="box-footer text-center">
            <button type="submit" class="btn btn-primary">Submit</button>
         </div>
      </form>
   </section>
   <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<script type="text/javascript">
   function deleteRow(obj) {
      $(obj).closest('tr').remove();
   }

   function addItemtoListAutoReply(){
      var row = '<tr> <td><input type="text" name="a_message[]" class="form-control" required></td><td><input type="text" name="a_response_time[]" class="form-control"></td><td><a onclick="deleteRow(this)" class="btn btn-danger"><i class=""></i> Delete </a></td></tr>';

      $("#a_tbl_body").append(row);
   }

   function addItemtoListIntReply(){
      var row = '<tr> <td><input type="text" name="i_message[]" class="form-control" required></td><td><input type="text" name="i_response_time[]" class="form-control"></td><td><a onclick="deleteRow(this)" class="btn btn-danger"><i class=""></i> Delete </a></td></tr>';

      $("#i_tbl_body").append(row);
   }

   function addItemtoListNewsLetter(){
      var row = '<tr> <td><input type="text" name="n_message[]" class="form-control"></td><td><input type="text" name="n_response_time[]" class="form-control"></td><td><a onclick="deleteRow(this)" class="btn btn-danger"><i class=""></i> Delete </a></td></tr>';

      $("#n_tbl_body").append(row);
   }
</script>
@endsection

