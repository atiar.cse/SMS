<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Campain;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $campains = Campain::where('flag', 1)->where('user_id', Auth::user()->id)->get();
        return view('SMS.campain.index', compact('campains'));
    }
    public function home()
    {
        $campains = Campain::where('flag', 1)->where('user_id', Auth::user()->id)->get();
        return view('SMS.campain.index', compact('campains'));
    }    
}
