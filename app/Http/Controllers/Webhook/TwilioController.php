<?php

namespace App\Http\Controllers\Webhook;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
// use Response;
// use Twilio\Rest\Client;
use App\Models\Gateway;
use App\Models\WebhookRespond;
use App\Models\WebhookMessage;
use App\Models\Campain;
use App\Models\CampainAutoMessage;

class TwilioController extends Controller
{

    public function respond(Request $request)
    {   
        //For twilio : phone should be with + and countryCode, need to store in DB with plus, countryCode

        $ToCountry = '';
        if (isset( $_POST['ToCountry'] )) {
            $ToCountry = $_POST['ToCountry'];
        }
        $ToState = '';
        if (isset( $_POST['ToState'] )) {
            $ToState = $_POST['ToState'];
        }
        $SmsMessageSid = '';
        if (isset( $_POST['SmsMessageSid'] )) {
            $SmsMessageSid = $_POST['SmsMessageSid'];
        }
        $NumMedia = '';
        if (isset( $_POST['NumMedia'] )) {
            $NumMedia = $_POST['NumMedia'];
        }
        $ToCity = '';
        if (isset( $_POST['ToCity'] )) {
            $ToCity = $_POST['ToCity'];
        }
        $FromZip = '';
        if (isset( $_POST['FromZip'] )) {
            $FromZip = $_POST['FromZip'];
        }
        $SmsSid = '';
        if (isset( $_POST['SmsSid'] )) {
            $SmsSid = $_POST['SmsSid'];
        }
        $SmsStatus = '';
        if (isset( $_POST['SmsStatus'] )) {
            $SmsStatus = $_POST['SmsStatus'];
        }
        $FromCity = '';
        if (isset( $_POST['FromCity'] )) {
            $FromCity = $_POST['FromCity'];
        }
        $MessageBody = '';
        if (isset( $_POST['Body'] )) {
            $MessageBody = $_POST['Body'];
        }
        $FromCountry = '';
        if (isset( $_POST['FromCountry'] )) {
            $FromCountry = $_POST['FromCountry'];
        }
        $gateway_phone_number = '+8801728'; ////*** Twilio Gateway phone number
        if (isset( $_POST['To'] )) {
            $gateway_phone_number = $_POST['To'];
        }
        $ToZip = '';
        if (isset( $_POST['ToZip'] )) {
            $ToZip = $_POST['ToZip'];
        }
        $NumSegments = '';
        if (isset( $_POST['NumSegments'] )) {
            $NumSegments = $_POST['NumSegments'];
        }
        $MessageSid = '';
        if (isset( $_POST['MessageSid'] )) {
            $MessageSid = $_POST['MessageSid'];
        } 
        $AccountSid = '';
        if (isset( $_POST['AccountSid'] )) {
            $AccountSid = $_POST['AccountSid'];
        } 
        $traffic_phone_number = '705638'; ////*** Traffic / End user phone number
        if (isset( $_POST['From'] )) {
            $traffic_phone_number = $_POST['From'];
        } 
        $ApiVersion = '';
        if (isset( $_POST['ApiVersion'] )) {
            $ApiVersion = $_POST['ApiVersion'];
        }

        //Traffic Subscribtion Check
        $traffic_status = 1;
        if(strtolower($MessageBody) == 'stop' || strtolower($MessageBody) == 'stopall' || strtolower($MessageBody) == 'unsubscribe' || strtolower($MessageBody) == 'cancel' || strtolower($MessageBody) == 'end' || strtolower($MessageBody) == 'quit'){
            $traffic_status = 0;
        }
        //End Traffic Subscribtion Check

        //Get Active Gateway Id in our DB & it should be unique in tbl_gateway with any status, also not changable
        $gateway_pk_id = 0;
        $gateway_status = 0; //0=Pending, 1=Running, 2=Paused, 3=Trash
        $gateway = Gateway::where('phone_number',$gateway_phone_number)->First();
        if ($gateway) {
            $gateway_pk_id = $gateway->id;
            $gateway_status = $gateway->status;
        }

        //Need to check if the traffic is already in our tbl_webhook_respond with that gateway phone number - if not insert it once
        $webhook_respond = WebhookRespond::where('gateway_phone_number',$gateway_phone_number)
                                    ->where('traffic_phone_number',$traffic_phone_number)
                                    ->First();
        

        $webhook_respond_pk_id = 0;
        if ($webhook_respond) { //already exist, update gateway pk
            $webhook_respond->gateway_id = $gateway_pk_id;
            $webhook_respond->traffic_status = $traffic_status;
            $webhook_respond->last_message_time = date('Y-m-d H:i:s');  
            $webhook_respond->save();

            $webhook_respond_pk_id = $webhook_respond->id;
        } else { //create new
            $new_webhook_respond = new WebhookRespond;
            $new_webhook_respond->gateway = 1; //1=Twilio
            $new_webhook_respond->gateway_id = $gateway_pk_id;
            $new_webhook_respond->last_message_time = date('Y-m-d H:i:s');
            $new_webhook_respond->gateway_phone_number = $gateway_phone_number;
            $new_webhook_respond->traffic_phone_number = $traffic_phone_number;
            $new_webhook_respond->save();

            $webhook_respond_pk_id = $new_webhook_respond->id;
        }



        //Insert the received message from twilio
        $webhook_message = new WebhookMessage;
        $webhook_message->webhook_respond_id = $webhook_respond_pk_id;
        $webhook_message->gateway_id = $gateway_pk_id;
        $webhook_message->in_out = 0; //0=Incoming, 1=Outgoing
        $webhook_message->gateway_status = $gateway_status; //0=Pending, 1=Running, 2=Paused, 3=Trash
        $webhook_message->delivery_type = 0; //Not applicable for incoming message

        $webhook_message->ToCountry = $ToCountry;
        $webhook_message->ToState = $ToState;
        $webhook_message->SmsMessageSid = $SmsMessageSid;
        $webhook_message->NumMedia = $NumMedia;
        $webhook_message->ToCity = $ToCity;
        $webhook_message->FromZip = $FromZip;
        $webhook_message->SmsSid = $SmsSid;
        $webhook_message->SmsStatus = $SmsStatus;
        $webhook_message->FromCity = $FromCity;
        $webhook_message->MessageBody = $MessageBody;
        $webhook_message->FromCountry = $FromCountry;
        $webhook_message->ToZip = $ToZip;
        $webhook_message->NumSegments = $NumSegments;
        $webhook_message->MessageSid = $MessageSid;
        $webhook_message->AccountSid = $AccountSid;
        $webhook_message->ApiVersion = $ApiVersion;
        $webhook_message->save();


        $gateway = Gateway::find($gateway_pk_id);

        $flag = 0;

        $campain_id = $gateway->campain_id;

        $webhook_message = WebhookMessage::where('webhook_respond_id', $webhook_respond_pk_id)->where('delivery_type', 1)->where('in_out', 1)->orderBy('id', 'DESC')->first();
        if($webhook_message){
            $campain_id = $webhook_message->campain_id;
            $message_id = $webhook_message->message_id;
            $message = CampainAutoMessage::where('flag', 1)->where('campain_id', $campain_id)->where('id', '>', $message_id)->orderBy('id')->first();
            if($message){
                $message_id = $message->id;
                $message_body = $message->a_message;
                $response_time = $message->a_response_time;

                $flag = 1;
            }
            // else{
            //     $message = CampainAutoMessage::where('flag', 1)->where('campain_id', $campain_id)->first();
            //     $message_id = $message->id;
            //     $message_body = $message->a_message;
            //     $response_time = $message->a_response_time;

            //     $send_message_time = date("Y-m-d h:i:s", time() + $response_time);
            // }
            // $webhook_message = WebhookMessage::find($webhook_message->id);
        }
        else{
            $message = CampainAutoMessage::where('flag', 1)->where('campain_id', $campain_id)->first();
            $campain_id = $message->campain_id;
            $message_id = $message->id;
            $message_body = $message->a_message;
            $response_time = $message->a_response_time;

            $flag = 1;

            // $send_message_time = date("Y-m-d h:i:s", time() + $response_time);
        }

        if($flag == 1){
            //Insert the received message from twilio
            $webhook_message = new WebhookMessage;
            $webhook_message->webhook_respond_id = $webhook_respond_pk_id;
            $webhook_message->gateway_id = $gateway_pk_id;
            $webhook_message->in_out = 1; //0=Incoming, 1=Outgoing
            $webhook_message->gateway_status = $gateway_status; //0=Pending, 1=Running, 2=Paused, 3=Trash
            $webhook_message->delivery_type = 1; //Not applicable for incoming message
            $webhook_message->send_message_time = date("Y-m-d H:i:s", time() + $response_time);
            $webhook_message->status = 0;
            $webhook_message->campain_id = $campain_id;
            $webhook_message->message_id = $message_id;

            $webhook_message->ToCountry = $ToCountry;
            $webhook_message->ToState = $ToState;
            $webhook_message->SmsMessageSid = $SmsMessageSid;
            $webhook_message->NumMedia = $NumMedia;
            $webhook_message->ToCity = $ToCity;
            $webhook_message->FromZip = $FromZip;
            $webhook_message->SmsSid = $SmsSid;
            $webhook_message->SmsStatus = $SmsStatus;
            $webhook_message->FromCity = $FromCity;
            $webhook_message->MessageBody = $message_body;
            $webhook_message->FromCountry = $FromCountry;
            $webhook_message->ToZip = $ToZip;
            $webhook_message->NumSegments = $NumSegments;
            $webhook_message->ApiVersion = $ApiVersion;
            $webhook_message->save();
        }
        

        // $xml = '<Response><Message>Hello - your app just answered the phone. Neat, eh?</Message></Response>';
        // $response = Response::make($xml, 200);
        // $response->header('Content-Type', 'text/xml');
        // return $response;
    }
}
