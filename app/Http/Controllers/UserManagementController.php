<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;

class UserManagementController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        if(Auth::user()->type == 3){
            abort(404, 'Unauthorized Access');
        }
        $pending_users = User::where('flag', 0)->where('type', 3)->get();
        $approved_users = User::where('flag', 1)->where('type', 3)->get();
        $not_approved_users = User::where('flag', 2)->where('type', 3)->get();
        $banned_users = User::where('flag', 3)->where('type', 3)->get();

        return view('user.userList.index', compact('pending_users', 'approved_users', 'not_approved_users', 'banned_users'));
    }
    public function create()
    {
        //
    }
    public function store(Request $request)
    {
        //
    }
    public function show($id)
    {
        //
    }
    public function edit($id)
    {
        if(isset($_GET["action"]) && $_GET["action"]=='approve'){
            $user = User::find($id);
            if(is_null($user)){
                return redirect('userManagement');
            }
            $user->flag = 1;
            $user->save();
            return redirect('userManagement');
        }
        if(isset($_GET["action"]) && $_GET["action"]=='notApprove'){
            $user = User::find($id);
            if(is_null($user)){
                return redirect('userManagement');
            }
            $user->flag = 2;
            $user->save();
            return redirect('userManagement');
        }
        if(isset($_GET["action"]) && $_GET["action"]=='ban'){
            $user = User::find($id);
            if(is_null($user)){
                return redirect('userManagement');
            }
            $user->flag = 3;
            $user->save();
            return redirect('userManagement');
        }
        if(isset($_GET["action"]) && $_GET["action"]=='unban'){
            $user = User::find($id);
            if(is_null($user)){
                return redirect('userManagement');
            }
            $user->flag = 1;
            $user->save();
            return redirect('userManagement');
        }
    }
    public function update(Request $request, $id)
    {
        //
    }
    public function destroy($id)
    {
        //
    }
}
