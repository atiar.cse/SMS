<?php

namespace App\Http\Controllers\SMS;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\SendSMS;
use App\Models\CampainAutoMessage;
use App\Models\SMSNumber;
use TomLingham\Searchy\Facades\Searchy;
use App\Models\WebhookRespond;
use App\Models\Gateway;
use App\Models\WebhookMessage;
use App\Models\CampainNewsLetterMessage;
use App\Models\Twilio;
use Auth;

class SendSMSController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $flag = 1;
        $contact_number = '8801711111113';
        $message = 'int';
        $sms_number = '16222';
        $smsNumber = SMSNumber::where('sms_number', $sms_number)->where('user_id', Auth::user()->id)->first();
        if(!$smsNumber){
            print_r('Message Number Not exist in database');
            exit();
        }

        $sms_number_id = $smsNumber->id;
        $smsNumber = SMSNumber::find($sms_number_id);
        if(!$smsNumber){
            print_r('Error');
            exit();
        }

        $sendSMS = sendSMS::where('contact_number', $contact_number)->where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->first();//Check this Contact number exist in sendSMS table
        if($sendSMS){
            //If Number exist
            //this is not the first sms from this number
            $date_time = $sendSMS->date_time;//last sendSMS datetime
            $date_time = date('Y-m-d H:i:s', strtotime($date_time. ' + 7 days'));//add 7 days with last sendSMS datetime
            $current_date_time = date('Y-m-d H:i:s');//take current datetime
            //check last sendSMS datetime + 7 days with current datetime
            if($date_time > $current_date_time){
                //last sendSMS datetime is less than 7 days ago
                if($sendSMS->campain_id == $smsNumber->campain_id){
                    $campainMessage = CampainAutoMessage::where('flag', 1)->where('user_id', Auth::user()->id)->where('campain_id', $sendSMS->campain_id)->where('id', '>', $sendSMS->campain_message_id)->orderBy('id')->first();
                    //check campain message list end or not
                    if($campainMessage){
                        //if not end
                        $campain_message_id = $campainMessage->id;
                    }else{
                        //if end
                        $campainMessage = CampainAutoMessage::where('flag', 1)->where('user_id', Auth::user()->id)->where('campain_id', $sendSMS->campain_id)->orderBy('id')->first();
                        $campain_message_id = $campainMessage->id;
                    }
                }
                else{
                    $flag = 0;
                }
            }
            else{
                //last sendSMS datetime is more than 7 days ago
                $campainMessage = CampainAutoMessage::where('flag', 1)->where('user_id', Auth::user()->id)->where('campain_id', $sendSMS->campain_id)->orderBy('id')->first();
                    $campain_message_id = $campainMessage->id;
            }
        }
        else{
            //this is the first sms from this number
            $campainMessage = CampainAutoMessage::where('flag', 1)->where('user_id', Auth::user()->id)->where('campain_id', $smsNumber->campain_id)->orderBy('id')->first();
            $campain_message_id = $campainMessage->id;
        }

        if($flag == 1){
            //Insert data into sendSMS table
            $sendSMS = new sendSMS();
            $sendSMS->sms_number_id = $sms_number_id;
            $sendSMS->contact_number = $contact_number;
            $sendSMS->campain_id = $smsNumber->campain_id;
            $sendSMS->campain_message_id = $campain_message_id;
            $sendSMS->flag = 1;
            $sendSMS->user_id = Auth::user()->id;
            $sendSMS->date = date('Y-m-d');
            $sendSMS->date_time = date('Y-m-d H:i:s');
            $sendSMS->save();

            $campainMessage = CampainAutoMessage::find($campain_message_id);

            print_r($campainMessage->a_message);
            print_r("<br/>");
            print_r($campainMessage->getcampainInfoRow->name);
            print_r("<br/>");
            print_r($smsNumber->sms_number);
        }
        else{
            print_r('Campain Miss Match Error');
        }

        //Best Match Message
        $best_match = Searchy::campain_i_messages('i_message')->query($message)->get();

        if(!$best_match->isEmpty()){
            print_r("<br/>");
            $flag = 1;
            foreach($best_match as $rowdata){
                if($rowdata->campain_id == $sendSMS->campain_id && $flag == 1 ){
                    echo $rowdata->i_message;
                    $flag = 0;
                }
            }
            
        }

    }
    public function create()
    {
        $webhook_messages = WebhookMessage::where('gateway_status', 1)->where('in_out', 1)->where('status', 0)->where('send_message_time', '<=', date('Y-m-d H:i:s'))->get();
        print_r($webhook_messages); exit;
        if($webhook_messages){
            foreach($webhook_messages as $rowdata){
                $gateway = Gateway::find($rowdata->gateway_id);

                $sid = $gateway->sid_key;
                $token = $gateway->token_secret;

                $webhook_respond = WebhookRespond::find($rowdata->webhook_respond_id);

                $my_number = $webhook_respond->traffic_phone_number;
                $gateway_number = $webhook_respond->gateway_phone_number;
                $message_body = $rowdata->MessageBody;

                $result = new Twilio;
                $result->http_post = $message;
                $result->save();

                
                $webhook_message = WebhookMessage::find($rowdata->id);
                $webhook_message->status = 1;
                $webhook_message->MessageSid = 1;
                $webhook_message->AccountSid = 2;
                $webhook_message->save();                   
            }
        }

    }
    public function store(Request $request)
    {
        //
    }
    public function show($id)
    {
        $best_match = Searchy::campain_i_messages('i_message')->query($message)->get();
        if($best_match){
            print_r("<br/>");
            echo $best_match[0]->i_message;
        }
    }
    public function edit($id)
    {
        //
    }
    public function update(Request $request, $id)
    {
        //
    }
    public function destroy($id)
    {
        //
    }
}
