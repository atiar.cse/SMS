<?php

namespace App\Http\Controllers\SMS;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\Models\Campain;
use App\Models\CampainAutoMessage;
use App\Models\CampainIntelligenceMessage;
use App\Models\CampainNewsLetterMessage;

class CampainController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $campains = Campain::where('flag', 1)->where('user_id', Auth::user()->id)->get();

        return view('SMS.campain.index', compact('campains'));
    }
    public function create()
    {
        return view('SMS.campain.create');
    }
    public function store(Request $request)
    {
        // $this->validate($request, [
        //     'budget_code_id' => 'required',
        //     'budget_subhead_code' => 'required',
        //     'budget_subhead' => 'required'
        // ]);

        $group_name = $request->input('group_name');

        $a_message = $request->input('a_message');//Array
        $a_response_time = $request->input('a_response_time');//Array
        $i_message = $request->input('i_message');//Array
        $i_response_time = $request->input('i_response_time');//Array
        $n_message = $request->input('n_message');//Array
        $n_response_time = $request->input('n_response_time');//Array

        $response = Campain::create($request->all());
        $response->user_id = Auth::user()->id;
        $response->save();


        $arrData = array();
        foreach ( $a_message as $key => $value ) {
            $arrData[] = array(
                "campain_id" => $response->id,
                "flag" => 1,
                "user_id" => Auth::user()->id,
                "a_message" => $a_message[$key],
                "a_response_time" => $a_response_time[$key],
            );
        }
        $response_auto = CampainAutoMessage::insert($arrData);

        $arrData = array();
        foreach ( $i_message as $key => $value ) {
            $arrData[] = array(
                "campain_id" => $response->id,
                "flag" => 1,
                "user_id" => Auth::user()->id,
                "i_message" => $i_message[$key],
                "i_response_time" => $i_response_time[$key],
            );
        }
        $response_int = CampainIntelligenceMessage::insert($arrData);

        $arrData = array();
        foreach ( $n_message as $key => $value ) {
            $arrData[] = array(
                "campain_id" => $response->id,
                "flag" => 1,
                "user_id" => Auth::user()->id,
                "n_message" => $n_message[$key],
                "n_response_time" => $n_response_time[$key],
            );
        }
        $response_news = CampainNewsLetterMessage::insert($arrData);

        session()->flash('success', 'Data successfully saved');
        return redirect('/campain');
    }
    public function show($id)
    {
        $campain = Campain::find($id);

        return view('SMS.campain.show', compact('campain'));
    }
    public function edit($id)
    {
        $campain = Campain::find($id);

        return view('SMS.campain.edit', compact('campain'));
    }

    public function update(Request $request, $id)
    {
        $campain = Campain::find($id);
        $newTask = $campain->replicate();
        $newTask->flag = 0;
        $newTask->parent_id = $id;
        $newTask->save();

        $campain = Campain::find($id)->update($request->all());

        $a_message_id = $request->input('a_message_id'); //Array
        $i_message_id = $request->input('i_message_id'); //Array
        $n_message_id = $request->input('n_message_id'); //Array
        // print_r($project_fun_checklist_id); exit;
        //Auto Reply Message
        CampainAutoMessage::where('campain_id', $id)->where('flag', 1)->whereNotIn('id', $a_message_id)->update(['flag' => 0]);
        //Intelligence Message
        CampainIntelligenceMessage::where('campain_id', $id)->where('flag', 1)->whereNotIn('id', $i_message_id)->update(['flag' => 0]);
        //NewsLetter Message
        CampainNewsLetterMessage::where('campain_id', $id)->where('flag', 1)->whereNotIn('id', $n_message_id)->update(['flag' => 0]);
        
        $a_message = $request->input('a_message');//Array
        $a_response_time = $request->input('a_response_time');//Array
        $i_message = $request->input('i_message');//Array
        $i_response_time = $request->input('i_response_time');//Array
        $n_message = $request->input('n_message');//Array
        $n_response_time = $request->input('n_response_time');//Array

        //Auto Reply message Update
        $arrData = array();
        foreach ( $a_message as $key => $value ) {
            if($a_message_id[$key] < 0){
                $arrData[] = array(
                    "campain_id" => $id,
                    "flag" => 1,
                    "user_id" => Auth::user()->id,
                    "a_message" => $a_message[$key],
                    "a_response_time" => $a_response_time[$key],
                );
            }
            else{
                $response1 = CampainAutoMessage::find($a_message_id[$key]);
                $response1->a_message = $a_message[$key];
                $response1->a_response_time = $a_response_time[$key];
                $response1->save();
            }
        }
        CampainAutoMessage::insert($arrData);
        //End Auto Reply message Update

        //Intelligence Reply message Update
        $arrData = array();
        foreach ( $i_message as $key => $value ) {
            if($i_message_id[$key] < 0){
                $arrData[] = array(
                    "campain_id" => $id,
                    "flag" => 1,
                    "user_id" => Auth::user()->id,
                    "i_message" => $i_message[$key],
                    "i_response_time" => $i_response_time[$key],
                );
            }
            else{
                $response1 = CampainIntelligenceMessage::find($i_message_id[$key]);
                $response1->i_message = $i_message[$key];
                $response1->i_response_time = $i_response_time[$key];
                $response1->save();
            }
        }
        CampainIntelligenceMessage::insert($arrData);
        //End Intelligence Reply message Update

        //NewsLetter Reply message Update
        $arrData = array();
        foreach ( $n_message as $key => $value ) {
            if($n_message_id[$key] < 0){
                $arrData[] = array(
                    "campain_id" => $id,
                    "flag" => 1,
                    "user_id" => Auth::user()->id,
                    "n_message" => $n_message[$key],
                    "n_response_time" => $n_response_time[$key],
                );
            }
            else{
                $response1 = CampainNewsLetterMessage::find($n_message_id[$key]);
                $response1->n_message = $n_message[$key];
                $response1->n_response_time = $n_response_time[$key];
                $response1->save();
            }
        }
        CampainNewsLetterMessage::insert($arrData);
        //End NewsLetter Reply message Update

        session()->flash('success', 'Data successfully saved');
        return redirect('/campain');
    }

    public function destroy($id)
    {
        //
    }

    public function officeBudgetCode()
    {
        $budgetHeads = BudgetCode::where('flag', 1)->orderBy('budget_code','ASC')->get();
        return view('Budget.budgetSubhead.officeBudgetCode', compact('budgetHeads'));        
    }

    public function updateOfficeBudgetCode(Request $request)
    {
        $result = OfficeBudgetCode::where('flag',1)
                            ->where('office_id',Auth::user()->office_id)
                            ->First();
        if (!is_null($result)) {
            OfficeBudgetCode::where('flag',1)
                                ->where('office_id',Auth::user()->office_id)
                                ->update(['flag'=>0]);
        }

        $budget_subhead_id = $request->input('budget_subhead_id');
        $budget_code_id = $request->input('budget_code_id');
        if ($budget_subhead_id) {

                $arrData = array();
                foreach ( $budget_subhead_id as $key => $value ) {
                    $arrData[] = array(
                        'budget_code_id' => $budget_code_id[$value],
                        'budget_subhead_id' => $budget_subhead_id[$key],
                        'office_id' => Auth::user()->office_id,
                        'added_by' => Auth::id(),
                        "created_at" => date("Y-m-d H:i:s"),
                        "updated_at" => date("Y-m-d H:i:s"),

                    );
                }
                OfficeBudgetCode::insert($arrData);

        }

        session()->flash('success', 'Successfully saved.');
        return back();
    }
}
