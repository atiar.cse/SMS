<?php

namespace App\Http\Controllers\SMS;

use App\Http\Controllers\Controller;

use App\Models\Gateway;
use App\Models\Campain;
use Illuminate\Http\Request;
use Auth;

class GatewayController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $gateways = Gateway::where('flag', 1)->where('user_id', Auth::user()->id)->get();
        return view('SMS.gateway.index', compact('gateways'));
    }
    public function create()
    {
        $campains = Campain::where('flag', 1)->where('user_id', Auth::user()->id)->get();
        $campain_count = Campain::where('flag', 1)->where('user_id', Auth::user()->id)->count();

        return view('SMS.gateway.create', compact('campains', 'campain_count'));
    }
    public function store(Request $request)
    {   
        $phone_number = $request->input('phone_number');

        $gateway = Gateway::where('phone_number', $phone_number)->count();
        if($gateway > 0){
            session()->flash('error', 'This '.$phone_number.' number already exist in system. Please Contact with Support Center.');

            return back();
        }

        $response = Gateway::create($request->all());
        $response->user_id = Auth::user()->id;
        $response->save();

        return redirect('/gateway');
    }
    public function show($id)
    {
        if($_GET['status']){
            $status = $_GET['status'];

            $gateway = Gateway::find($id);
            if(is_null($gateway)){
                abort(404, 'Page Not Found');
            }
            $gateway->status = $status;
            $gateway->save();

            session()->flash('success', 'Status Changed successfully.');

            return back();
        }

        session()->flash('error', 'Problem to change Status successfully.');
        return back();
    }
    public function edit($id)
    {
        $gateway = Gateway::find($id);

        $campains = Campain::where('flag', 1)->where('user_id', Auth::user()->id)->get();
        return view('SMS.gateway.edit', compact('gateway', 'campains'));
    }
    public function update(Request $request, $id)
    {
        $response = Gateway::find($id)->update($request->all());
        return redirect('/gateway');
    }
    public function destroy($id)
    {
        //
    }
}
