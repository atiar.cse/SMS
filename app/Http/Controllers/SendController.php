<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Twilio\Rest\Client;
use App\Models\Twilio;
class SendController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
                
// +8801723857462 Tushar
// +8801737390079 bayzid
// +8801741607831

                // Your Account SID and Auth Token from twilio.com/console
                $sid = 'AC2051744f054ccb4e99a31892dc895fd4';
                $token = '95a63c0ece200b0e1acf4bbd903bffb9';
                $client = new Client($sid, $token);

                // Use the client to do fun stuff like send text messages!
            $message = $client->messages->create(
                    // the number you'd like to send the message to
                    '+8801728705638',
                    array(
                        // A Twilio phone number you purchased at twilio.com/console
                        'from' => 'XXX+18448575725',
                        // the body of the text message you'd like to send
                        'body' => 'Hey! This is test twilio messaage from localhost without.. +!'
                    )
                );
            $result = new Twilio;
            $result->http_post = $message;
            $result->save();

            print $message->sid;
            print $message->accountSid;

            print_r($message);
    }

    public function quick_send_test()
    {
        // Your Account SID and Auth Token from twilio.com/console
        $sid = 'AC2051744f054ccb4e99a31892dc895fd4';
        $token = '95a63c0ece200b0e1acf4bbd903bffb9';
        $client = new Client($sid, $token);

            // Use the client to do fun stuff like send text messages!
        $message = $client->messages->create(
                // the number you'd like to send the message to
                '+8801741607831',
                array(
                    // A Twilio phone number you purchased at twilio.com/console
                    'from' => '+18448575725',
                    // the body of the text message you'd like to send
                    'body' => 'Hey! This is test twilio messaage from Server Quick Test!'
                )
            );
        $result = new Twilio;
        $result->http_post = $message;
        $result->save();

        print $message->sid;
        print $message->accountSid;

        print_r($message);        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
