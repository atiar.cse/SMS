<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CampainNewsLetterMessage extends Model
{
    protected $table = 'campain_n_messages';
    protected $guarded = ['id'];

    public function getCampainInfoRow()
    {
        return $this->belongsTo('App\Models\Group', 'group_id', 'id');
    }
}
