<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WebhookMessage extends Model
{
    protected $table = 'webhook_messages';
    protected $guarded = ['id'];
}
