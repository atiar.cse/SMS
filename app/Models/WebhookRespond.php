<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WebhookRespond extends Model
{
    protected $table = 'webhook_respond';
    protected $guarded = ['id'];
}
