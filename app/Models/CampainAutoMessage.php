<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CampainAutoMessage extends Model
{
    protected $table = 'campain_a_messages';
    protected $guarded = ['id'];

    public function getCampainInfoRow()
    {
        return $this->belongsTo('App\Models\Campain', 'campain_id', 'id');
    }
}
