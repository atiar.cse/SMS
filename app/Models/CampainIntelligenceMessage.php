<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CampainIntelligenceMessage extends Model
{
    protected $table = 'campain_i_messages';
    protected $guarded = ['id'];

    public function getCampainInfoRow()
    {
        return $this->belongsTo('App\Models\Group', 'group_id', 'id');
    }
}
