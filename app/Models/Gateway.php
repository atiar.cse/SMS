<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Gateway extends Model
{
    protected $table = 'gateway';
    protected $guarded = ['id'];

    public function getCampainInfoRow()
    {
        return $this->belongsTo('App\Models\Campain', 'campain_id', 'id');
    }
}
