<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WebhookDeliveryStats extends Model
{
    protected $table = 'webhook_delivery_stats';
    protected $guarded = ['id'];
}
