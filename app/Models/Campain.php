<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Campain extends Model
{
    protected $table = 'campains';
    protected $fillable = ['name', 'flag', 'user_id', 'parent_id', 'campain_action'];

    public function getAutoMessageInfoRow()
    {
        return $this->hasMany('App\Models\CampainAutoMessage', 'campain_id', 'id')->where('flag', 1);
    }
    public function getIntelligenceMessageInfoRow()
    {
        return $this->hasMany('App\Models\CampainIntelligenceMessage', 'campain_id', 'id')->where('flag', 1);
    }
    public function getNewsLetterMessageInfoRow()
    {
        return $this->hasMany('App\Models\CampainNewsLetterMessage', 'campain_id', 'id')->where('flag', 1);
    }
}
