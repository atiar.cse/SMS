<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

use Twilio\Rest\Client;

use App\Models\Test;
use App\Models\WebhookMessage;
use App\Models\Gateway;
use App\Models\Campain;
use App\Models\CampainNewsLetterMessage;
use App\Models\WebhookRespond;
use App\Models\Twilio;

class Kernel extends ConsoleKernel
{
    protected $commands = [
        
    ];
    protected function schedule(Schedule $schedule)
    {   
        // FYI - Please don't remove this test command
        $schedule->call(function () {
            $current_date = date('d/m/Y == H:i:s');
            $text = 'Hey - current time is ';
            $text .= $current_date;

            $response = new Test;
            $response->name = $text;
            $response->save();
        })->everyMinute();
        // FYI - Please don't remove this test command

        //START Auto Reply - Twilio API
        $schedule->call(function () {

            $webhook_messages = WebhookMessage::where('gateway_status', 1)
                                    ->where('in_out', 1)
                                    ->where('status', 0)
                                    // ->where('gateway', 1) Need to create this column to db tbl, 1=Twilio
                                    ->where('send_message_time', '<=', date('Y-m-d H:i:s'))
                                    ->get();
            if($webhook_messages){
                foreach($webhook_messages as $rowdata){
                    $webhook_respond = WebhookRespond::find($rowdata->webhook_respond_id);

                    if($webhook_respond->traffic_status == 1){

                        $gateway = Gateway::find($rowdata->gateway_id);

                        $sid = $gateway->sid_key;
                        $token = $gateway->token_secret;

                        $my_number = $webhook_respond->traffic_phone_number;
                        $gateway_number = $webhook_respond->gateway_phone_number;
                        $message_body = $rowdata->MessageBody;

                        $result = new Twilio;
                        $result->http_post = $message_body;
                        $result->traffic_phone_number = $my_number;
                        $result->gateway_number = $gateway_number;
                        $result->webhook_message_id = $rowdata->id;
                        $result->result = '2'; //2=Before try - test record
                        $result->save();

                        //Also we need to change the status of the tbl_webhook_messages record as attempted to deliver. Because to avoid it for next cron job if faild anyway. 
                        $webhook_message = WebhookMessage::find($rowdata->id);
                        $webhook_message->status = 4;//attempted to deliver
                        $webhook_message->save();                        

                        try {
                            $client = new Client($sid, $token);

                            $message = $client->messages->create(
                                $my_number,
                                array(
                                    'from' => $gateway_number,
                                    'body' => $message_body
                                )
                            );

                            // $result = new Twilio;
                            // // $result->http_post = $message;
                            // $result->traffic_phone_number = $my_number;
                            // $result->gateway_number = $gateway_number;
                            // $result->webhook_message_id = $rowdata->id;
                            // $result->result = '1';
                            // $result->save();

                            if($message){
                                $webhook_message = WebhookMessage::find($rowdata->id); //this may be no needed again
                                $webhook_message->status = 1;//success
                                $webhook_message->MessageSid = $message->sid;
                                $webhook_message->AccountSid = $message->accountSid;
                                $webhook_message->save();
                            }
                            else
                            {
                                $webhook_message = WebhookMessage::find($rowdata->id);  //this may be no needed again
                                $webhook_message->status = 2;//fail
                                $webhook_message->save();
                            }
                        } catch(Exception $e){

                            $result = new Twilio;
                            // $result->http_post = $message;
                            $result->traffic_phone_number = $my_number;
                            $result->gateway_number = $gateway_number;
                            $result->webhook_message_id = $rowdata->id;
                            $result->result = '0';
                            $result->save();
                        }
                    }
                    else{
                        $webhook_message = WebhookMessage::find($rowdata->id);
                        $webhook_message->status = 3;//stopped by end user
                        $webhook_message->save();
                    }
                }
            }
        })->everyMinute();
        //END Auto Reply - Twilio API

        //NewsLetter Message
        $schedule->call(function () {
            $webhook_responds = WebhookRespond::where('is_newsletter', '!=', 1)->get();

            $arrData = array();
            if($webhook_responds){
                foreach($webhook_responds as $rowdata){
                    $gateway = Gateway::find($rowdata->gateway_id);
                    if($gateway){
                        $campain_id = $gateway->campain_id;

                        $webhook_message = WebhookMessage::where('webhook_respond_id', $rowdata->id)->orderBy('id', 'DESC')->first();

                        $newsletter = CampainNewsLetterMessage::where('flag', 1)->where('campain_id', $campain_id)->orderBy('id')->first();
                        if (!empty($newsletter)) {
                            $n_response_time = $newsletter->n_response_time;

                            $newsletter_time = date("Y-m-d H:i:s", time() - $n_response_time);

                            if($newsletter_time >= $rowdata->last_message_time){
                                $newsletter_messages = CampainNewsLetterMessage::where('flag', 1)->where('campain_id', $campain_id)->get();
                                if($newsletter_messages){
                                    $next_message_time = $rowdata->last_message_time;
                                    foreach($newsletter_messages as $rowdata2){

                                        $send_message_time = date("Y-m-d h:i:s", strtotime('+'.$rowdata2->n_response_time.' seconds',strtotime($next_message_time)));

                                        $arrData[] = array(
                                            "webhook_respond_id" => $rowdata->id,
                                            "gateway_id" => $rowdata->gateway_id,
                                            "in_out" => 1,
                                            "gateway_status" => $gateway->status,
                                            "delivery_type" => 3,
                                            "send_message_time" => $send_message_time,
                                            "status" => 0,
                                            "campain_id" => $campain_id,
                                            "message_id" => $rowdata2->id,

                                            "MessageBody" => $rowdata2->n_message,
                                        );
                                        $next_message_time = $send_message_time;
                                    }
                                }
                                WebhookRespond::where('id', $rowdata->id)->update(['is_newsletter' => 1]);
                            }
                        }                 
                    }

                }
            }

            WebhookMessage::insert($arrData);
        })->everyMinute();
        //End NewsLetter


        // $schedule->command('TwilioCommand')->everyFiveMinutes();
        // $schedule->command('TwilioCommand')->everyMinute();
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
