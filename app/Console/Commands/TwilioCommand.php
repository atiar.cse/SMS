<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use Twilio\Rest\Client;
use App\Models\Twilio;

use App\Models\Gateway;
use App\Models\Campain;
use App\Models\WebhookRespond;
use App\Models\WebhookMessage;
use App\Models\WebhookDeliveryStats;

use App\Models\Test;

class TwilioCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Gateway:Twilio';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Auto send - twilio webhook campain messages reply';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //Twilio campain auto responder steps :-
        // 1. get all active twilio gateway from tbl_gateway, gateway=1
        // 2. get all this gateway related traffic from tbl_webhook_respond
        // 3. check if already newsletter activated for the traffic from the tbl_webhook_delivery_stats by count > 0
        // 4. If not newsletter running then send next auto reply by counting from tbl_webhook_delivery_stats for this traffic and campain
        // 5. Also send intelligence reply for this traffic if matched the keyword. 



        // $schedule->call(function () {
        //     $current_date = date('d/m/Y == H:i:s');
        //     $text = 'Hey - Twillo Command ';
        //     $text .= $current_date;

        //     $response = new Test;
        //     $response->name = $text;
        //     $response->save();
        // })->everyMinute();





        // // Your Account SID and Auth Token from twilio.com/console
        // $sid = 'AC2051744f054ccb4e99a31892dc895fd4';
        // $token = '95a63c0ece200b0e1acf4bbd903bffb9';
        // $client = new Client($sid, $token);

        //     // Use the client to do fun stuff like send text messages!
        // $message = $client->messages->create(
        //     // the number you'd like to send the message to
        //     '+8801741607831',
        //     array(
        //         // A Twilio phone number you purchased at twilio.com/console
        //         'from' => '+18448575725',
        //         // the body of the text message you'd like to send
        //         'body' => 'Hey! This is Every 5 Min test! Rakib'
        //     )
        // );


        // $result = new Twilio;
        // $result->http_post = $message;
        // $result->save();





        // $webhook_messages = WebhookMessage::where('gateway_status', 1)->where('in_out', 1)->where('delivery_type', 1)->where('status', 0)->where('send_message_time', '<=', date('Y-m-d H:i:s'))->get();
        // if($webhook_messages){
        //     foreach($webhook_messages as $rowdata){
        //         $gateway = Gateway::find($rowdata->gateway_id);

        //         $sid = $gateway->sid_key;
        //         $token = $gateway->token_secret;

        //         $webhook_respond = WebhookRespond::find($rowdata->webhook_respond_id);

        //         $client = new Client($sid, $token);

        //         // Use the client to do fun stuff like send text messages!
        //         $message = $client->messages->create(
        //             // the number you'd like to send the message to
        //             $webhook_respond->traffic_phone_number,
        //             array(
        //                 // A Twilio phone number you purchased at twilio.com/console
        //                 'from' => $webhook_respond->gateway_phone_number,
        //                 // the body of the text message you'd like to send
        //                 'body' => $rowdata->MessageBody
        //             )
        //         );
        //         $result = new Twilio;
        //         $result->http_post = $message;
        //         $result->save();

        //         $webhook_message = WebhookMessage::find($rowdata->id);
        //         $webhook_message->status = 1;
        //         $webhook_message->save();
        //     }
        // }
    }
}
