-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 10, 2018 at 06:04 PM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bulksms`
--

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `id` int(11) NOT NULL,
  `flag` int(11) NOT NULL DEFAULT '1',
  `parent_id` int(11) DEFAULT NULL,
  `contact` varchar(20) NOT NULL,
  `remarks` text,
  `group_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`id`, `flag`, `parent_id`, `contact`, `remarks`, `group_id`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, '8801711111111', 'this is test remarks', 6, '2018-03-10 10:48:00', '2018-03-10 10:48:00'),
(2, 1, NULL, '8801711111112', 'Updated remarks', 1, '2018-03-10 10:53:44', '2018-03-10 10:56:07'),
(3, 1, NULL, '8801711111113', 'as asdf asdf asdf', 6, '2018-03-10 10:53:55', '2018-03-10 10:53:55'),
(4, 1, NULL, '8801711111112', 'This is updated remarks', 1, '2018-03-10 10:55:04', '2018-03-10 10:55:04'),
(5, 0, 2, '8801711111112', 'asdf asd sadf asfas asdf', 1, '2018-03-10 10:56:07', '2018-03-10 10:56:07'),
(6, 1, NULL, '8801711111114', 'this is remarks', 1, '2018-03-10 10:59:15', '2018-03-10 10:59:15');

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` int(11) NOT NULL,
  `flag` int(11) NOT NULL DEFAULT '1',
  `parent_id` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `group_action` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `flag`, `parent_id`, `name`, `group_action`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, 'group - 12', 1, '2018-03-10 15:34:59', '2018-03-10 09:34:59'),
(2, 0, 1, 'group - 1', 1, '2018-03-10 15:40:10', '2018-03-10 09:34:59'),
(3, 0, 1, 'group - 12', 1, '2018-03-10 15:40:09', '2018-03-10 09:35:37'),
(4, 0, 1, 'group - 12', 1, '2018-03-10 15:39:59', '2018-03-10 09:36:00'),
(5, 0, 1, 'group - 12', 1, '2018-03-10 09:39:37', '2018-03-10 09:39:37'),
(6, 1, NULL, 'group - 1', 1, '2018-03-10 10:15:33', '2018-03-10 10:15:33');

-- --------------------------------------------------------

--
-- Table structure for table `group_messages`
--

CREATE TABLE `group_messages` (
  `id` int(11) NOT NULL,
  `flag` int(11) NOT NULL DEFAULT '1',
  `group_id` int(11) NOT NULL,
  `message` text NOT NULL,
  `response_time` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `group_messages`
--

INSERT INTO `group_messages` (`id`, `flag`, `group_id`, `message`, `response_time`, `created_at`, `updated_at`) VALUES
(1, 0, 1, 'hi', 12, '2018-03-10 15:02:23', '2018-03-10 09:34:59'),
(2, 0, 1, 'hello', 45, '2018-03-10 15:02:23', '2018-03-10 09:34:59'),
(3, 0, 1, 'hi', 12, '2018-03-10 15:36:00', '2018-03-10 09:39:37'),
(4, 0, 1, 'hello', 45, '2018-03-10 15:36:00', '2018-03-10 09:39:37'),
(5, 0, 1, 'How are you?', 120, '2018-03-10 15:36:00', '2018-03-10 09:39:37'),
(6, 1, 1, 'hi', 12, '2018-03-10 15:39:37', '2018-03-10 15:39:37'),
(7, 1, 1, 'hello', 45, '2018-03-10 15:39:37', '2018-03-10 15:39:37'),
(8, 1, 1, 'How are you?', 120, '2018-03-10 15:39:37', '2018-03-10 15:39:37'),
(9, 1, 6, 'Hi', 40, '2018-03-10 16:15:33', '2018-03-10 16:15:33'),
(10, 1, 6, 'How are you?', 45, '2018-03-10 16:15:33', '2018-03-10 16:15:33'),
(11, 1, 6, 'Do you have your dinner?', 90, '2018-03-10 16:15:33', '2018-03-10 16:15:33');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `group_messages`
--
ALTER TABLE `group_messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `group_messages`
--
ALTER TABLE `group_messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
