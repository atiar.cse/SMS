-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 20, 2018 at 08:59 PM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bulksms`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` int(11) NOT NULL,
  `flag` int(11) NOT NULL DEFAULT '0' COMMENT '0=pending, 1=approved, 2=not approved, 3=banned',
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `type`, `flag`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Md. Rakibul Islam', 1, 1, 'rakib@bulksms.com', '$2y$10$k6ZksLT9yYdXaidi4z4bCurAmXIHJ3VEGBrX32dfoKaqW5gIxSseS', 'AVAN10Qac1nFORZ4qHbaDfvoMeh5PCNKORWpScUnyZYvAhqAXym1aR6NLC5k', '2018-03-20 17:45:23', '2018-03-20 17:45:23'),
(2, 'Md. Rakibul Islam', 1, 0, 'rakib1@bulksms.com', '$2y$10$VduCzERC3fjgUvK61M8EyOsGWYQstXK4Rx0M.M9DmggDC9HOK38p2', '2MGVs2bFYG9DEIoQkiZLzqLoBCfvXXP8gcZKKBRpuEX14z70NRW4S9aIC6QH', '2018-03-20 18:05:58', '2018-03-20 18:05:58'),
(3, 'Md. Rakibul Islam', 2, 0, 'admin@gmail.com', '$2y$10$315E32dVOh0Fo6AFBM9aNO8twWdXWhOYiQjzQVmylNgouwryzfHA.', 'Uh57AbRfb6fjGjB9lMv8HNu9nauNqk58CSUD0r68ZOtTYTIeAhQRksG9hS4z', '2018-03-20 19:13:15', '2018-03-20 19:13:15'),
(4, 'Md. Rakibul Islam', 3, 1, 'admin1@gmail.com', '$2y$10$Mer9OOhwcgiNJJhgNmk/bumUPoBiZCWWXQDJEmUcTw9JP50EWQ8Qi', 'FZXMhUmPFt5XWxgHGAFHbTdIZYMU4ShI1RciOi4rf7RIHUCMTfyJZrF5dWA6', '2018-03-20 19:32:44', '2018-03-20 19:54:18'),
(5, 'Md. Rakibul Islam', 3, 3, 'rakib2@bulksms.com', '$2y$10$cUM2mbmtK1lUthH2o2ZrHeWZroWougFsZe.r9aAOp3PiITE3eiA8G', 'a2BeIl0IQqpb7qHGSmiJmF4eiNMR2UREMvR18wSmHHxJeUmcunTbhKacNczC', '2018-03-20 19:54:55', '2018-03-20 19:55:17');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
