-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 19, 2018 at 07:34 PM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bulksms`
--

-- --------------------------------------------------------

--
-- Table structure for table `campains`
--

CREATE TABLE `campains` (
  `id` int(11) NOT NULL,
  `flag` int(11) NOT NULL DEFAULT '1',
  `parent_id` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `campains`
--

INSERT INTO `campains` (`id`, `flag`, `parent_id`, `name`, `created_at`, `updated_at`) VALUES
(10, 1, NULL, 'Campain - 02', '2018-03-19 18:05:08', '2018-03-19 18:05:08'),
(11, 0, 10, 'Campain - 01', '2018-03-19 17:46:12', '2018-03-19 17:46:12'),
(12, 0, 10, 'Campain - 01', '2018-03-19 17:46:30', '2018-03-19 17:46:30'),
(13, 0, 10, 'Campain - 01', '2018-03-19 17:47:23', '2018-03-19 17:47:23'),
(14, 0, 10, 'Campain - 01', '2018-03-19 17:54:03', '2018-03-19 17:54:03'),
(15, 0, 10, 'Campain - 01', '2018-03-19 17:55:30', '2018-03-19 17:55:30'),
(16, 0, 10, 'Campain - 01', '2018-03-19 17:55:45', '2018-03-19 17:55:45'),
(17, 0, 10, 'Campain - 01', '2018-03-19 17:57:24', '2018-03-19 17:57:24'),
(18, 0, 10, 'Campain - 01', '2018-03-19 17:57:50', '2018-03-19 17:57:50'),
(19, 0, 10, 'Campain - 01', '2018-03-19 17:58:25', '2018-03-19 17:58:25'),
(20, 0, 10, 'Campain - 01', '2018-03-19 17:59:35', '2018-03-19 17:59:35'),
(21, 0, 10, 'Campain - 01', '2018-03-19 18:00:44', '2018-03-19 18:00:44'),
(22, 0, 10, 'Campain - 01', '2018-03-19 18:05:08', '2018-03-19 18:05:08'),
(23, 0, 10, 'Campain - 02', '2018-03-19 18:06:40', '2018-03-19 18:06:40'),
(24, 0, 10, 'Campain - 02', '2018-03-19 18:07:10', '2018-03-19 18:07:10'),
(25, 1, NULL, 'Campain - 03', '2018-03-19 18:17:51', '2018-03-19 18:17:51'),
(26, 0, 25, 'Campain - 03', '2018-03-19 18:19:36', '2018-03-19 18:19:36');

-- --------------------------------------------------------

--
-- Table structure for table `campain_a_messages`
--

CREATE TABLE `campain_a_messages` (
  `id` int(11) NOT NULL,
  `flag` int(11) NOT NULL DEFAULT '1',
  `campain_id` int(11) NOT NULL,
  `a_message` text NOT NULL,
  `a_response_time` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `campain_a_messages`
--

INSERT INTO `campain_a_messages` (`id`, `flag`, `campain_id`, `a_message`, `a_response_time`, `created_at`, `updated_at`) VALUES
(1, 1, 10, 'Auto Reply Message - 01', 120, '2018-03-19 17:19:48', '2018-03-19 17:19:48'),
(2, 1, 10, 'Auto Reply Message - 02', 130, '2018-03-19 17:19:48', '2018-03-19 18:05:08'),
(3, 0, 10, 'Auto Reply Message - 01', 120, '2018-03-19 17:19:48', '2018-03-19 17:57:24'),
(4, 0, 10, 'Auto Reply Message - 01', 120, '2018-03-19 17:19:48', '2018-03-19 17:57:24'),
(5, 0, 10, 'Auto Reply Message - 03', 100, '2018-03-19 18:05:08', '2018-03-19 18:06:40'),
(6, 0, 10, 'Auto Reply Message - 04', 150, '2018-03-19 18:05:08', '2018-03-19 18:06:40'),
(7, 0, 10, 'Auto Reply Message - 03', 100, '2018-03-19 18:06:40', '2018-03-19 18:07:10'),
(8, 1, 10, 'Auto Reply Message - 04', 150, '2018-03-19 18:06:40', '2018-03-19 18:06:40'),
(9, 1, 10, 'Auto Reply Message - 05', NULL, '2018-03-19 18:07:10', '2018-03-19 18:07:10'),
(10, 1, 25, 'Auto Reply Message - 01', 100, '2018-03-19 18:17:51', '2018-03-19 18:19:36'),
(11, 1, 25, 'Auto Reply Message - 02', 110, '2018-03-19 18:17:51', '2018-03-19 18:19:36'),
(12, 1, 25, 'Auto Reply Message - 03', 120, '2018-03-19 18:17:51', '2018-03-19 18:19:36');

-- --------------------------------------------------------

--
-- Table structure for table `campain_i_messages`
--

CREATE TABLE `campain_i_messages` (
  `id` int(11) NOT NULL,
  `flag` int(11) NOT NULL DEFAULT '1',
  `campain_id` int(11) NOT NULL,
  `i_message` text NOT NULL,
  `i_response_time` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `campain_i_messages`
--

INSERT INTO `campain_i_messages` (`id`, `flag`, `campain_id`, `i_message`, `i_response_time`, `created_at`, `updated_at`) VALUES
(1, 1, 10, 'Intelligence Message - 01', 110, '2018-03-19 17:19:48', '2018-03-19 17:19:48'),
(2, 1, 10, 'Intelligence Message - 01', 110, '2018-03-19 17:19:48', '2018-03-19 17:19:48'),
(3, 1, 10, 'Intelligence Message - 03', 100, '2018-03-19 18:07:10', '2018-03-19 18:07:10'),
(4, 1, 25, 'Intelligence Message - 01', 200, '2018-03-19 18:17:51', '2018-03-19 18:19:36'),
(5, 1, 25, 'Intelligence Message - 02', 210, '2018-03-19 18:17:51', '2018-03-19 18:19:36'),
(6, 1, 25, 'Intelligence Message - 03', 220, '2018-03-19 18:17:51', '2018-03-19 18:19:36'),
(7, 1, 25, 'Intelligence Message - 04', 230, '2018-03-19 18:19:36', '2018-03-19 18:19:36');

-- --------------------------------------------------------

--
-- Table structure for table `campain_n_messages`
--

CREATE TABLE `campain_n_messages` (
  `id` int(11) NOT NULL,
  `flag` int(11) NOT NULL DEFAULT '1',
  `campain_id` int(11) NOT NULL,
  `n_message` text NOT NULL,
  `n_response_time` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `campain_n_messages`
--

INSERT INTO `campain_n_messages` (`id`, `flag`, `campain_id`, `n_message`, `n_response_time`, `created_at`, `updated_at`) VALUES
(1, 1, 10, 'News Letter Message - 01', 320, '2018-03-19 17:19:48', '2018-03-19 17:19:48'),
(2, 1, 10, 'News Letter Message - 01', 320, '2018-03-19 17:19:48', '2018-03-19 17:19:48'),
(3, 1, 10, 'News Letter Message - 01', 320, '2018-03-19 17:19:48', '2018-03-19 17:19:48'),
(4, 1, 10, 'News Letter Message - 04', 342, '2018-03-19 18:07:10', '2018-03-19 18:07:10'),
(5, 1, 25, 'News Letter Message - 01', 300, '2018-03-19 18:17:51', '2018-03-19 18:19:36'),
(6, 1, 25, 'News Letter Message - 02', 310, '2018-03-19 18:17:51', '2018-03-19 18:19:36'),
(7, 1, 25, 'News Letter Message - 03', 320, '2018-03-19 18:17:51', '2018-03-19 18:19:37'),
(8, 1, 25, 'News Letter Message - 04', 400, '2018-03-19 18:19:37', '2018-03-19 18:19:37');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sendsms`
--

CREATE TABLE `sendsms` (
  `id` int(11) NOT NULL,
  `status` int(11) DEFAULT NULL COMMENT '1=success, 0= fail',
  `sms_number_id` int(11) NOT NULL,
  `contact_number` varchar(50) NOT NULL,
  `group_id` int(11) NOT NULL,
  `group_message_id` int(11) NOT NULL,
  `flag` int(11) NOT NULL,
  `date` date NOT NULL,
  `date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sendsms`
--

INSERT INTO `sendsms` (`id`, `status`, `sms_number_id`, `contact_number`, `group_id`, `group_message_id`, `flag`, `date`, `date_time`, `created_at`, `updated_at`) VALUES
(32, NULL, 7, '8801711111112', 1, 6, 1, '2018-03-18', '2018-03-17 18:48:20', '2018-03-17 18:48:20', '2018-03-17 18:48:20'),
(33, NULL, 7, '8801711111112', 1, 7, 1, '2018-03-18', '2018-03-17 19:02:46', '2018-03-17 19:02:46', '2018-03-17 19:02:46'),
(34, NULL, 7, '8801711111112', 1, 8, 1, '2018-03-18', '2018-03-17 19:02:49', '2018-03-17 19:02:49', '2018-03-17 19:02:49'),
(35, NULL, 7, '8801711111112', 1, 6, 1, '2018-03-18', '2018-03-17 19:02:51', '2018-03-17 19:02:51', '2018-03-17 19:02:51'),
(36, NULL, 4, '8801711111112', 1, 7, 1, '2018-03-18', '2018-03-17 19:07:56', '2018-03-17 19:07:56', '2018-03-17 19:07:56'),
(37, NULL, 7, '8801711111112', 1, 8, 1, '2018-03-18', '2018-03-17 19:33:56', '2018-03-17 19:33:56', '2018-03-17 19:33:56'),
(38, NULL, 7, '8801711111112', 1, 6, 1, '2018-03-18', '2018-03-17 19:33:58', '2018-03-17 19:33:58', '2018-03-17 19:33:58'),
(39, NULL, 7, '8801711111112', 1, 7, 1, '2018-03-18', '2018-03-17 20:42:40', '2018-03-17 20:42:40', '2018-03-17 20:42:40'),
(40, NULL, 7, '8801711111112', 1, 8, 1, '2018-03-18', '2018-03-17 20:42:55', '2018-03-17 20:42:55', '2018-03-17 20:42:55'),
(41, NULL, 7, '8801711111112', 1, 6, 1, '2018-03-18', '2018-03-17 20:43:22', '2018-03-17 20:43:22', '2018-03-17 20:43:22'),
(42, NULL, 7, '8801711111112', 1, 7, 1, '2018-03-18', '2018-03-17 21:25:49', '2018-03-17 21:25:49', '2018-03-17 21:25:49'),
(43, NULL, 7, '8801711111112', 1, 8, 1, '2018-03-18', '2018-03-17 21:27:19', '2018-03-17 21:27:19', '2018-03-17 21:27:19');

-- --------------------------------------------------------

--
-- Table structure for table `sms_numbers`
--

CREATE TABLE `sms_numbers` (
  `id` int(11) NOT NULL,
  `flag` int(11) NOT NULL DEFAULT '1',
  `parent_id` int(11) DEFAULT NULL,
  `sms_number` varchar(20) NOT NULL,
  `remarks` text,
  `campain_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sms_numbers`
--

INSERT INTO `sms_numbers` (`id`, `flag`, `parent_id`, `sms_number`, `remarks`, `campain_id`, `created_at`, `updated_at`) VALUES
(10, 1, NULL, '16222', 'Govt. Application Number', 10, '2018-03-19 18:27:15', '2018-03-19 18:32:15'),
(11, 0, 10, '16222', 'Govt. Application Number', 10, '2018-03-19 18:32:05', '2018-03-19 18:32:05'),
(12, 0, 10, '16223', 'Govt. Application Number 9', 25, '2018-03-19 18:32:15', '2018-03-19 18:32:15');

-- --------------------------------------------------------

--
-- Table structure for table `test`
--

CREATE TABLE `test` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `test`
--

INSERT INTO `test` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'test', '2018-03-12 13:31:40', '2018-03-12 13:31:40'),
(2, 'test', '2018-03-12 13:34:16', '2018-03-12 13:34:16');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `campains`
--
ALTER TABLE `campains`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `campain_a_messages`
--
ALTER TABLE `campain_a_messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `campain_i_messages`
--
ALTER TABLE `campain_i_messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `campain_n_messages`
--
ALTER TABLE `campain_n_messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sendsms`
--
ALTER TABLE `sendsms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sms_numbers`
--
ALTER TABLE `sms_numbers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `test`
--
ALTER TABLE `test`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `campains`
--
ALTER TABLE `campains`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `campain_a_messages`
--
ALTER TABLE `campain_a_messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `campain_i_messages`
--
ALTER TABLE `campain_i_messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `campain_n_messages`
--
ALTER TABLE `campain_n_messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sendsms`
--
ALTER TABLE `sendsms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT for table `sms_numbers`
--
ALTER TABLE `sms_numbers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `test`
--
ALTER TABLE `test`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
